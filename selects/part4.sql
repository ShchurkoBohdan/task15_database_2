#(Статистичні функції та робота з групами)
#1
SELECT model, price FROM Printer
where price = any (select max(price) from Printer);

#2
select model, speed from Laptop 
where speed < all (select speed from PC);

#3
select maker, price from Product
join Printer on Product.model = Printer.model
where price <= all (select price from Printer);