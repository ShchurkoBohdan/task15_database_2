#(Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)
#1
SELECT maker FROM Product
where model not in (select model from Laptop) and type = 'PC';

#2
SELECT maker FROM Product
where model = all (select model from PC);

#3
SELECT maker FROM Product
where model = any (select model from PC);

#4
SELECT distinct maker FROM Product
where type in ('PC','Laptop');

#5
SELECT maker FROM Product
where model = all (select model from PC) 
and model = all (select model from Laptop);

#6
SELECT maker FROM Product
where model = any (select model from PC) 
or model = any (select model from Laptop);