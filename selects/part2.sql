#(Використання підзапитів з лог. операцією EXISTS)
#1
SELECT maker FROM Product
where exists (select model from PC where model = Product.model); 

#2
SELECT maker FROM Product
where exists (select model from PC where model = Product.model and speed >= 750);

#3
SELECT maker FROM Product
where exists (select model from PC where model = Product.model and speed >= 750)
and exists(select model from PC where model = Product.model and speed >= 750);

#4
SELECT distinct maker FROM Product
where exists (select model from PC where model = Product.model and speed > any (select speed from PC)) 
