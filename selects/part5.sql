#(Підзапити у якості обчислювальних стовпців)
#1
select distinct maker, (select count(model) from Product where type = 'pc') pc, 
(select count(model) from Product where type = 'laptop') laptop,
(select count(model) from Product where type = 'printer') printer from Product;

#(Оператор CASE)
#1
select distinct maker, 
	case 
		when (select count(maker) from Product where type = 'pc') > 0 then concat('YES(',
			(select count(model) from PC where model = Product.model), ')')
		when (select count(maker) from Product where type = 'pc') = 0 then 'NO'
		else 'incorrect'
	end as pc
from Product;

#(Об’єднання UNION)
#1
select maker, model, type from Product
union
select code, model, price from PC
union 
select code, model, price from Printer
union
select code, model, price from Laptop;
