create schema if not exists students_db;
use students_db ;

create table if not exists `students_db`.`address` (
  `id` int not null auto_increment primary key,
  `country` varchar(45) not null,
  `city` varchar(45) not null,
  `street` varchar(45) not null,
  `house_number` int not null,
  `flat_number` int not null)
engine = InnoDB;

create table if not exists `students_db`.`group` (
  `id` int not null auto_increment primary key,
  `code` varchar(45) not null,
  `speciality` varchar(45) not null)
engine = InnoDB;


create table if not exists `students_db`.`student` (
  `id` int not null auto_increment primary key,
  `name` varchar(45) not null,
  `photo` longblob null default null,
  `autobiographi` varchar(45) null default null,
  `year_of_entry` int not null,
  `birthday` date not null,
  `current_rating` decimal(5,2) not null,
  `address_id` int not null,
  `group_id` int not null,
  constraint `fk_student_address1`
    foreign key (`address_id`)
    references `students_db`.`address` (`id`)
    on delete no action
    ON update no action,
  constraint `fk_student_group1`
    foreign key (`group_id`)
    references `students_db`.`group` (`id`)
    on delete no action
    ON update no action)
engine = InnoDB;


create table if not exists `students_db`.`subject` (
  `id` int not null auto_increment primary key,
  `name` varchar(45) not null unique)
engine = InnoDB;



create table if not exists `students_db`.`student_subject` (
  `student_id` int not null,
  `subject_id` int not null,
  `result_of_module_1` decimal(5,2) not null,
  `result_of_module_2` decimal(5,2) not null,
  `grade_in_100_points` decimal(5,2) not null,
  `grade_in_5_points` decimal(5,2) not null,
  `teacher` varchar(45) not null,
  `number_of_semester_for_subject` int not null,
  `type_of_control` varchar(6) not null CHECK (`type_of_control` IN ('credit', 'test')),
  PRIMARY KEY (`grade_in_100_points`, `subject_id`, `student_id`),
  constraint `fk_student_subject_subject`
    foreign key (`subject_id`)
    references `students_db`.`subject` (`id`)
    on delete no action
    ON update no action,
  constraint `fk_student_subject_student1`
    foreign key (`student_id`)
    references `students_db`.`student` (`id`)
    on delete no action
    ON update no action)
engine = InnoDB;

insert into `students_db`.`address`
values (1, 'Ukraine', 'Lviv', 'Naukova', 21, 908),
	   (3, 'Ukraine', 'Kharkiv', 'Stepana Bandery', 234, 2),
       (2, 'Ukraine', 'Kyiv', 'Khreshchatyk', 12, 08);


insert into `students_db`.`group`
values (1, 'FK-11', 'applied math'),
	   (2, 'FK-12', 'informatics');


insert into `students_db`.`student`
(`id`, `name`, `year_of_entry`, `birthday`, `current_rating`, `address_id`, `group_id`)
values (1, 'Igor', 2017, '1999-11-06', 87.5, 1, 1),
	   (2, 'Sasha', 2017, '2000-07-16', 72, 1, 1),
       (3, 'Misha', 2017, '1999-12-14', 65, 2, 2);

insert into `students_db`.`subject`
values (1, 'Sociology'),
	   (2, 'Theology'),
       (3, 'Biology');
       
insert into `students_db`.`student_subject`
values (1, 1, 20, 20, 81, 4, 'Bohdan', 2, 'test'),
	   (1, 2, 25, 22, 90, 5, 'Eugeniy', 2, 'test'),
       (1, 3, 15, 20, 74, 4, 'Ihor', 1, 'credit'),
       (2, 1, 14, 20, 71, 4, 'Bohdan', 2, 'test'),
	   (2, 2, 15, 12, 61, 3, 'Eugeniy', 2, 'test'),
       (2, 3, 11, 15, 53, 3, 'Ihor', 1, 'credit'),
       (3, 1, 14, 18, 64, 3, 'Bohdan', 2, 'test'),
	   (3, 2, 15, 25, 73, 4, 'Eugeniy', 2, 'test'),
       (3, 3, 14, 18, 56, 3, 'Ihor', 1, 'credit');